package unittesting;

import static org.junit.Assert.*;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PersonTest {
	static Person person;
	static Person person2, person3;
	static SpendingAccount acc;
	static SavingAccount acc1;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		person=new Person(2,"Alina","1234567890");
		person2=new Person(2,"Alina","1234567890");
		person3=new Person(3,"Alina","1234567890");
		acc=new SpendingAccount(6,person2,500,150);
		acc1=new SavingAccount(5,person3,500);
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		person=null;
		person2=null;
		person3=null;
		acc=null;
		acc1=null;
		}

	@Test
	public void testGetID() {
		assertEquals(2,person.getID());
	}

	@Test
	public void testGetName() {
		assertEquals("Alina",person.getName());
	}

	@Test
	public void testGetCNP() {
		assertEquals("1234567890",person.getCNP());
	}
	@Test
	public void testWithdraw()
	{
		acc.withdraw(20);
		assertEquals(480,acc.getBalance());
	}
	@Test 
	public void testDeposit()
	{
		acc.deposit(20);
		assertEquals(500,acc.getBalance());
	}
	@Test
	public void testDepositSaving()
	{
		acc1.deposit(20);
		assertEquals(520,acc1.getBalance());
	}
}
