package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import model.Account;
import model.Bank;
import model.SavingAccount;
import model.SpendingAccount;

@SuppressWarnings("serial")
/**
 * 
 */
public class MyAccount extends JPanel implements ActionListener {

	JLabel msg1, msg2, aid, aidF, balance, balanceF, pin, pinA, type, typeF, max, maxF;
	JLabel idLog, pinLog;
	JTextField idLogF, pinLogF;
	private JButton depositB, withdrawB, ok;
	JTextField deposit, withdraw;
	private Account account1;
	private Bank bank;

	public MyAccount(Bank bank) {
		this.bank = bank;
		Border border = BorderFactory.createRaisedBevelBorder();

		setLayout(null);
		setSize(700, 750);
		setOpaque(false);

		idLogF = new JTextField("");
		idLogF.setBounds(250, 200, 150, 25);
		add(idLogF);

		pinLogF = new JTextField("");
		pinLogF.setBounds(250, 240, 150, 25);
		add(pinLogF);

		Font font = new Font("Verdana", Font.BOLD, 12);

		idLog = new JLabel("Account ID: ");
		idLog.setFont(font);
		idLog.setBounds(100, 200, 150, 30);
		add(idLog);

		pinLog = new JLabel("PIN:");
		pinLog.setFont(font);
		pinLog.setBounds(100, 240, 150, 30);
		add(pinLog);

		ok = new JButton("OK");
		ok.setBounds(300, 270, 100, 25);
		ok.setBackground(new Color(209, 230, 125));
		ok.setForeground(new Color(3, 54, 73));
		ok.setBorder(border);
		ok.addActionListener(this);
		add(ok);

	}

	private void displayInfo() {
		Font font = new Font("Verdana", Font.BOLD, 14);
		Border border = BorderFactory.createRaisedBevelBorder();

		msg1 = new JLabel("Welcome " + account1.getPerson().getName() + "!");
		msg1.setFont(font);
		add(msg1);
		msg1.setBounds(30, 30, 300, 30);

		msg2 = new JLabel("Here is your account information:");
		msg2.setFont(font);
		add(msg2);
		msg2.setBounds(30, 60, 300, 30);

		type = new JLabel("Type:");
		type.setFont(font);
		add(type);
		type.setBounds(100, 100, 100, 30);

		String s;
		if (account1 instanceof SavingAccount)
			s = "Saving account";
		else
			s = "Spending account";

		typeF = new JLabel(s);
		typeF.setFont(font);
		add(typeF);
		typeF.setBounds(200, 100, 200, 30);

		aid = new JLabel("ID:");
		aid.setFont(font);
		add(aid);
		aid.setBounds(100, 160, 100, 30);

		aidF = new JLabel("" + account1.getID());
		aidF.setFont(font);
		add(aidF);
		aidF.setBounds(200, 160, 100, 30);

		pin = new JLabel("PIN:");
		pin.setFont(font);
		add(pin);
		pin.setBounds(100, 130, 100, 30);

		pinA = new JLabel("" + account1.getPIN());
		pinA.setFont(font);
		add(pinA);
		pinA.setBounds(200, 130, 100, 30);

		balance = new JLabel("Balance: ");
		balance.setFont(font);
		add(balance);
		balance.setBounds(100, 190, 100, 30);

		balanceF = new JLabel("" + account1.getBalance() + " RON");
		balanceF.setFont(font);
		add(balanceF);
		balanceF.setBounds(200, 190, 300, 30);

		String s1 = "", s2 = "";
		if (account1 instanceof SpendingAccount) {
			s1 = "Max withdraw sum:";
			s2 = "" + ((SpendingAccount) account1).getMaxWithdraw();
		} else {
			s1 = "Interest (1.6%):";
			s2 = "" + ((SavingAccount) account1).getInterest();
		}

		max = new JLabel(s1);
		max.setFont(font);
		add(max);
		max.setBounds(20, 220, 180, 30);

		maxF = new JLabel(s2 + " RON");
		maxF.setFont(font);
		add(maxF);
		maxF.setBounds(200, 220, 300, 30);

		depositB = new JButton("Deposit");
		add(depositB);
		depositB.setBounds(100, 300, 100, 25);
		depositB.setBackground(new Color(209, 230, 125));
		depositB.setForeground(new Color(3, 54, 73));
		depositB.setBorder(border);
		depositB.addActionListener(this);

		withdrawB = new JButton("Withdraw");
		add(withdrawB);
		withdrawB.setBounds(100, 340, 100, 25);
		withdrawB.setBackground(new Color(209, 230, 125));
		withdrawB.setForeground(new Color(3, 54, 73));
		withdrawB.setBorder(border);
		withdrawB.addActionListener(this);

		deposit = new JTextField();
		add(deposit);
		deposit.setBounds(240, 300, 100, 30);

		withdraw = new JTextField();
		add(withdraw);
		withdraw.setBounds(240, 340, 100, 30);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == ok) {
			try {
			account1 = bank.getAccount(Integer.parseInt(idLogF.getText()));
			if ((account1 != null) && (account1.getPIN() == Integer.parseInt(pinLogF.getText()))) {
				ok.setVisible(false);
				idLogF.setVisible(false);
				pinLogF.setVisible(false);
				idLog.setVisible(false);
				pinLog.setVisible(false);
				displayInfo();
			} else
				sendMessage("Wrong ID or PIN. Try again!");
			bank.updateAccount(bank.getAccount(Integer.parseInt(idLogF.getText())), account1);
			}
			catch (Exception e1)
			{
				sendMessage("ID or PIN were not given as integers");
			}
		}
		if (e.getSource() == depositB) {
			try {
			int sum = Integer.parseInt(deposit.getText());
			account1.deposit(sum);
			bank.updateAccount(bank.getAccount(Integer.parseInt(idLogF.getText())), account1);
			removeAll();
			displayInfo();
			}
			catch (Exception e1)
			{
				sendMessage("Sum was not given as an integer");
			}
		}
		if (e.getSource() == withdrawB) {
			int sum = Integer.parseInt(withdraw.getText());
			if (sum > account1.getBalance())
				sendMessage("Not enough money to withdraw!");
			else if ((account1 instanceof SpendingAccount) && (sum > ((SpendingAccount) account1).getMaxWithdraw()))
				sendMessage(
						"The sum is greater than the Maximum sum available to withdraw.\n 		Enter a sum less than "
								+ ((SpendingAccount) account1).getMaxWithdraw() + ".");

			else {
				account1.withdraw(sum);
				bank.updateAccount(bank.getAccount(Integer.parseInt(idLogF.getText())), account1);
				removeAll();
				displayInfo();
			}
		}

	}

	private void sendMessage(String msg) {
		JOptionPane.showMessageDialog(null, msg, "Results", JOptionPane.PLAIN_MESSAGE);
	}
}
