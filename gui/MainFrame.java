package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import model.Account;
import model.Bank;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

@SuppressWarnings({ "unused", "serial" })
/**
 * 
 */
public class MainFrame extends JFrame implements ActionListener {

	private BackgroundPanel backPanel;
	private JPanel toolsPanel, infoPanel;
	private JButton myAcc, report, createAcc, admin;
	private AccountsPanel accPanel;
	private AdminPanel p;
	private ReportPanel reportPane;
	private MyAccount acc;
	public Bank bank;
	public Account account;
	private JButton saveToFile;

	public MainFrame(Bank bank) {

		this.bank = bank;

		Border border = BorderFactory.createRaisedBevelBorder();

		setSize(1000, 600);
		setResizable(false);
		setLayout(null);
		setLocation(120, 30);

		backPanel = new BackgroundPanel();
		setContentPane(backPanel);
		backPanel.setBounds(0, 0, 1000, 700);

		toolsPanel = new JPanel();
		toolsPanel.setSize(300, 700);
		toolsPanel.setLayout(null);
		toolsPanel.setBounds(0, 0, 230, 700);
		toolsPanel.setOpaque(false);

		infoPanel = new JPanel();
		infoPanel.setSize(700, 700);
		infoPanel.setLayout(null);
		infoPanel.setBounds(220, 0, 750, 700);
		infoPanel.setOpaque(false);

		myAcc = new JButton("My Account");
		myAcc.setBounds(50, 230, 140, 30);
		myAcc.setBackground(new Color(209, 230, 125));
		myAcc.setForeground(new Color(3, 54, 73));
		myAcc.setBorder(border);
		myAcc.addActionListener(this);
		
		JLabel label1=new JLabel("Student Bank Simulator");
		Font font = new Font("Verdana", Font.BOLD, 48);
		label1.setFont(font);
		label1.setForeground(Color.BLACK);
		label1.setBounds(50,90,1000,40);
		

		report = new JButton("Bank Report");
		report.setBounds(50, 300, 140, 30);
		report.setBackground(new Color(209, 230, 125));
		report.setForeground(new Color(3, 54, 73));
		report.setBorder(border);
		report.addActionListener(this);

		createAcc = new JButton("Create your Account");
		createAcc.setBounds(50, 370, 140, 30);
		createAcc.setBackground(new Color(209, 230, 125));
		createAcc.setForeground(new Color(3, 54, 73));
		createAcc.setBorder(border);
		createAcc.addActionListener(this);

		admin = new JButton("Administrator");
		admin.setBounds(50, 440, 140, 30);
		admin.setBackground(new Color(209, 230, 125));
		admin.setForeground(new Color(3, 54, 73));
		admin.setBorder(border);
		admin.addActionListener(this);

		toolsPanel.add(myAcc);
		toolsPanel.add(report);
		toolsPanel.add(createAcc);
		toolsPanel.add(admin);

		backPanel.add(toolsPanel);
		backPanel.add(infoPanel);
		backPanel.add(label1);

		saveToFile = new JButton("Save to File");
		add(saveToFile);
		saveToFile.setBounds(50, 510, 140, 30);
		saveToFile.setBackground(new Color(209, 230, 125));
		saveToFile.setForeground(new Color(3, 54, 73));
		saveToFile.setBorder(border);
		saveToFile.addActionListener(this);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == myAcc) {
			acc = new MyAccount(bank);
			acc.setOpaque(false);
			infoPanel.removeAll();
			infoPanel.add(acc);
			acc.setBounds(0, 150, 750, 700);
			infoPanel.repaint();
			backPanel.repaint();
		}
		if (e.getSource() == report) {
			reportPane = new ReportPanel(bank);
			reportPane.setOpaque(false);
			infoPanel.removeAll();
			infoPanel.add(reportPane);
			reportPane.setBounds(0, 150, 750, 700);
			infoPanel.repaint();
			backPanel.repaint();
		}
		if (e.getSource() == createAcc) {
			accPanel = new AccountsPanel(bank);
			infoPanel.removeAll();
			infoPanel.add(accPanel);
			accPanel.setBounds(0, 150, 750, 700);
			infoPanel.repaint();
			backPanel.repaint();

		}
		if (e.getSource() == admin) {

			p = new AdminPanel(bank);
			infoPanel.removeAll();
			infoPanel.add(p);
			p.setBounds(0, 200, 750, 600);
			p.setOpaque(false);
			infoPanel.repaint();
			backPanel.repaint();

		}
		if (e.getSource() == saveToFile) {
			serialOut("bank.ser", bank);
			JOptionPane.showMessageDialog(null, "Changes made were saved succesfully");
		}
	}

	public static void serialOut(String file, Bank o) {

		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeObject(o);
			out.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
