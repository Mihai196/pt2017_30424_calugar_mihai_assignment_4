package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

@SuppressWarnings("serial")
/**
 * 
 */
public class AccountsPanel extends JPanel implements ActionListener {

	private JLabel msg;
	private JLabel name, aid, pid, cnp, type, max, intr, intrF;
	private JTextField nameF, aidF, cnpF, pidF, maxF;
	private JButton confirm, submit;
	private JRadioButton b1, b2;
	private ButtonGroup group;
	private int t;
	private Account account;
	private Bank bank;

	/**
	 * constructor
	 * 
	 * @param bank
	 *            bank on which the operations are performed
	 */
	public AccountsPanel(Bank bank) {
		this.bank = bank;

		setLayout(null);
		setSize(700, 750);
		setOpaque(false);
		Border border = BorderFactory.createRaisedBevelBorder();
		t = 0;
		msg = new JLabel("Fill the fields below to create your new account:");
		msg.setBounds(20, 20, 300, 30);
		add(msg);

		name = new JLabel("Name:");
		name.setBounds(30, 60, 150, 30);
		add(name);

		pid = new JLabel("YourID:");
		pid.setBounds(30, 100, 150, 30);
		add(pid);

		cnp = new JLabel("Your CNP:");
		cnp.setBounds(30, 140, 150, 30);
		add(cnp);

		aid = new JLabel("Account ID:");
		aid.setBounds(30, 180, 150, 30);
		add(aid);

		type = new JLabel("Account type:");
		type.setBounds(30, 220, 150, 30);
		add(type);

		b1 = new JRadioButton("Savings Account");
		b1.setSelected(true);
		b1.addActionListener(this);
		b1.setBounds(190, 220, 150, 20);
		b1.setOpaque(false);
		add(b1);

		b2 = new JRadioButton("Spending Account");
		b2.addActionListener(this);
		b2.setBounds(190, 250, 150, 20);
		b2.setOpaque(false);
		add(b2);

		group = new ButtonGroup();
		group.add(b1);
		group.add(b2);

		max = new JLabel("Max withdraw sum:");
		max.setBounds(30, 280, 150, 30);
		max.setVisible(false);
		add(max);

		intr = new JLabel("Interest:");
		intr.setBounds(30, 280, 150, 30);
		intr.setVisible(false);
		add(intr);

		intrF = new JLabel(" 1.6 %");
		intrF.setBounds(190, 280, 100, 30);
		intrF.setVisible(false);
		add(intrF);

		nameF = new JTextField();
		nameF.setBounds(190, 60, 150, 30);
		add(nameF);

		pidF = new JTextField();
		pidF.setBounds(190, 100, 150, 30);
		add(pidF);

		cnpF = new JTextField();
		cnpF.setBounds(190, 140, 150, 30);
		add(cnpF);

		aidF = new JTextField();
		aidF.setBounds(190, 180, 150, 30);
		add(aidF);

		maxF = new JTextField();
		maxF.setBounds(190, 280, 150, 30);
		maxF.setVisible(false);
		add(maxF);

		maxF.setVisible(false);
		max.setVisible(false);
		intr.setVisible(true);
		intrF.setVisible(true);

		confirm = new JButton("Confirm");
		confirm.setBounds(200, 350, 100, 20);
		confirm.setBackground(new Color(209, 230, 125));
		confirm.setForeground(new Color(3, 54, 73));
		confirm.setBorder(border);
		confirm.addActionListener(this);
		add(confirm);

		submit = new JButton("Submit");
		submit.setBounds(200, 380, 100, 20);
		submit.setBackground(new Color(209, 230, 125));
		submit.setForeground(new Color(3, 54, 73));
		submit.setBorder(border);
		submit.addActionListener(this);
		add(submit);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b1) {
			maxF.setVisible(false);
			max.setVisible(false);
			intr.setVisible(true);
			intrF.setVisible(true);
			t = 0;

		}
		if (e.getSource() == b2) {
			maxF.setVisible(true);
			max.setVisible(true);
			intr.setVisible(false);
			intrF.setVisible(false);
			t = 1;
		}
		if (e.getSource() == confirm) {
			int idp, ida, maxw;
			String cnpp, namee;

			idp = Integer.parseInt(pidF.getText());
			ida = Integer.parseInt(aidF.getText());
			cnpp = cnpF.getText();
			namee = nameF.getText();

			if (t == 1) {
				maxw = Integer.parseInt(maxF.getText());
				Person person = new Person(idp, namee, cnpp);
				account = new SpendingAccount(ida, person, maxw);
			}
			if (t == 0) {

				Person person = new Person(idp, namee, cnpp);
				account = new SavingAccount(ida, person);
			}
		}
		if (e.getSource() == submit) {
			if (account != null) {
				bank.addAccount(account);
				JOptionPane.showMessageDialog(null, "New account was created successfully");
				account = null;
			}
		}

	}
}
