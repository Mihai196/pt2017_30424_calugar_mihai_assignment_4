package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;

import model.Account;
import model.Bank;
import model.HashEntry;
import model.HashTable;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

@SuppressWarnings("serial")
/**
 * 
 */
public class AdminPanel extends JPanel implements ActionListener {

	private Bank bank;
	private JTextField deleteF;
	private JLabel del;
	private JButton delete;
	private JTable banktable;
	private JScrollPane scrollPane;

	/**
	 * Constructor
	 * 
	 * @param bank
	 */
	public AdminPanel(Bank bank) {
		this.bank = bank;

		Border border = BorderFactory.createRaisedBevelBorder();

		setLayout(null);
		// setSize(700,750);
		setOpaque(false);

		deleteF = new JTextField();
		add(deleteF);
		deleteF.setBounds(450, 300, 70, 25);

		delete = new JButton("Delete");
		add(delete);
		delete.setBounds(530, 300, 80, 25);
		delete.setBackground(new Color(209, 230, 125));
		delete.setForeground(new Color(3, 54, 73));
		delete.setBorder(border);
		delete.addActionListener(this);

		del = new JLabel("ID:");
		add(del);
		del.setBounds(400, 300, 30, 25);

		seeAccounts();

	}

	private void seeAccounts() {
		Person p;
		Object a;
		int dim = bank.getTable().getDimension();
		HashTable hashTable = bank.getTable();
		HashEntry[] table = hashTable.getTable();
		int index = 0;
		String[] colName = { "A ID", "P ID", "Name", "CNP", "PIN", "Savings A", "Spending A ", "Balance" };
		Object[][] prod = new Object[dim][8];
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null)
				for (int j = 0; j < table[i].size() && (index < dim); j++) {

					a = table[i].getValue(j);
					if (a instanceof SavingAccount) {
						p = ((SavingAccount) a).getPerson();
						a = (SavingAccount) table[i].getValue(j);
					} else {
						p = ((SpendingAccount) a).getPerson();
						a = (SpendingAccount) table[i].getValue(j);
					}

					prod[index] = new Object[8];
					prod[index][0] = ((Account) a).getID();
					prod[index][1] = p.getID();
					prod[index][2] = p.getName();
					prod[index][3] = p.getCNP();
					prod[index][4] = ((Account) a).getPIN();

					prod[index][5] = (a instanceof SavingAccount);
					prod[index][6] = (a instanceof SpendingAccount);

					prod[index][7] = ((Account) a).getBalance();
					index++;

				}
			banktable = new JTable(prod, colName);
			banktable.setOpaque(false);
			banktable.getColumnModel().getColumn(0).setPreferredWidth(20);
			banktable.getColumnModel().getColumn(1).setPreferredWidth(20);
			banktable.getColumnModel().getColumn(2).setPreferredWidth(70);
			scrollPane = new JScrollPane(banktable);

			scrollPane.setBounds(0, 0, 750, 200);
			scrollPane.setOpaque(false);
			add(scrollPane);

		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			Account a = bank.getAccount(Integer.parseInt(deleteF.getText()));
			if (a != null) {
				removeAll();
				bank.removeAccount(a);
				seeAccounts();
			} else
				sendMessage("No account with that ID");
		}
	}

	private void sendMessage(String msg) {
		JOptionPane.showMessageDialog(null, msg, "Results", JOptionPane.PLAIN_MESSAGE);
	}
}
