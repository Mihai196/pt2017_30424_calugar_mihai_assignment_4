package gui;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Bank;
import model.HashEntry;
import model.HashTable;
import model.SavingAccount;
import model.SpendingAccount;

@SuppressWarnings("serial")
/**
 * 
 */
public class ReportPanel extends JPanel {
	/**
	 * Constructor
	 * 
	 * @param bank
	 *            bank on which the operations are performed
	 */
	public ReportPanel(Bank bank) {
		JLabel name, saveA, spendA, saveA1, spendA1, profit, profitA;

		Font font = new Font("Times New Roman", Font.BOLD, 16);
		Font font1 = new Font("Times New Roman", Font.BOLD, 14);

		HashTable hashTable = bank.getTable();
		HashEntry[] table = hashTable.getTable();

		name = new JLabel("Bank report");
		name.setFont(font);
		name.setBounds(30, 30, 400, 30);
		add(name);

		saveA = new JLabel("Total Saving Accounts:");
		saveA.setFont(font1);
		saveA.setBounds(30, 100, 350, 30);
		add(saveA);

		spendA = new JLabel("Total Spending Accounts:");
		spendA.setFont(font1);
		spendA.setBounds(30, 150, 350, 30);
		add(spendA);

		profit = new JLabel("Total balance of the bank:");
		profit.setFont(font1);
		profit.setBounds(30, 200, 350, 30);
		add(profit);

		Object a;
		int nrSave = 0, nrSpend = 0, sumSave = 0, sumSpend = 0;
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null)
				for (int j = 0; j < table[i].size(); j++) {
					a = table[i].getValue(j);
					if (a instanceof SavingAccount) {
						nrSave++;
						sumSave += ((SavingAccount) a).getBalance();
					}
					if (a instanceof SpendingAccount) {
						nrSpend++;
						sumSpend += ((SpendingAccount) a).getBalance();
					}
				}

		}

		profitA = new JLabel("" + (sumSpend + sumSave) + " RON");
		profitA.setFont(font1);
		profitA.setBounds(300, 200, 450, 30);
		add(profitA);

		saveA1 = new JLabel("" + nrSave + " accounts with value of " + sumSave + " RON");
		saveA1.setFont(font1);
		saveA1.setBounds(300, 100, 450, 30);
		add(saveA1);

		spendA1 = new JLabel("" + nrSpend + " accounts with value of " + sumSpend + " RON");
		spendA1.setFont(font1);
		spendA1.setBounds(300, 150, 450, 30);
		add(spendA1);
	}
}
