package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Person implements Serializable {
	private int ID;
	private String name;
	private String CNP;
	
	public Person()
	{
		this.ID=0;
		this.name="";
		this.CNP="";
	}
	public Person(int id, String name, String CNP) {
		assert id!=0;
		assert name!=null;
		assert CNP.length()==10;
		this.ID = id;
		this.name = name;
		this.CNP = CNP;
	}

	/**
	 * Set Person Id to id.
	 * 
	 * @param id
	 *            ID
	 */
	public void setID(int id) {
		this.ID = id;
	}

	/**
	 * Set name of Person to name.
	 * 
	 * @param name
	 *            Person's name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set CNP of Person to CNP.
	 * 
	 * @param CNP
	 *            Person's CNP
	 */
	public void setCNP(String CNP) {
		this.CNP = CNP;
	}

	/**
	 * Returns ID of the Person.
	 * 
	 * @return ID
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Returns name of the Person.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns CNP of the Person
	 * 
	 * @return CNP
	 */
	public String getCNP() {
		return CNP;
	}
	/**
	 * returns a string representation of the information stored in the object
	 */
	public String toString() {
		return "\nID: " + ID + "\n Name:" + name + "\n CNP:" + CNP ;
	}
}
