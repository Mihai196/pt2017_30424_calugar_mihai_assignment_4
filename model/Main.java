package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import gui.MainFrame;

public class Main {

	public static void main(String[] args) {

		/*HashTable t = new HashTable();

		Person a = new Person(1, "Gheorghe Andrei", "1892238989");
		Person b = new Person(2, "Ionita George", "1782343898");
		Person c = new Person(3, "Stefanescu Maria", "2872384845");
		Person d = new Person(4, "Pop Cristina", "2827364546");

		Account a1 = new SpendingAccount(1, a, 200, 20);
		Account b1 = new SpendingAccount(2, b, 23400, 240);
		Account c1 = new SavingAccount(3, c, 23470);
		Account d1 = new SpendingAccount(4, d, 24000, 300);

		String s = "";
		s += a1.toString() + "\n" + b1.toString() + "\n" + c1.toString() + "\n" + d1.toString() + "\n";
		System.out.println(s);

		System.out.println(t.toString());
		Bank bank = new Bank(t);

		bank.addAccount(a1);
		bank.addAccount(b1);
		bank.addAccount(c1);
		bank.addAccount(d1);

		serialOut("bank.ser", bank);
		System.out.println(bank.toString());*/
		
		Bank ba = new Bank();
		ba = serialIn("bank.ser");
		//System.out.println(ba.toString());
		//System.out.println(ba.getTable().getDimension());
		new MainFrame(ba);

	}

	public static void serialOut(String file, Bank o) {
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeObject(o);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Bank serialIn(String file) {
		Bank bank = new Bank();
		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			bank = (Bank) in.readObject();
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bank;
	}

}
