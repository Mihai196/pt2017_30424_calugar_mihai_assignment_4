package model;

import java.io.Serializable;
import java.util.Random;
import static java.lang.Math.*;

@SuppressWarnings("serial")
/**
 * 
 */
public class SavingAccount extends Account implements Serializable {
	private double interest;

	public SavingAccount() {

	}

	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person owning the account
	 * @param balance
	 *            the sum present in the account
	 */
	public SavingAccount(int i, Person p, int balance) {
		Random m = new Random();
		ID = i;
		person = p;
		this.balance = balance;
		PIN = abs(m.nextInt() % 9000) + 1000;
		interest = balance * 1.6 / 100;

	}

	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person awning the account
	 */
	public SavingAccount(int i, Person p) {
		Random m = new Random();
		ID = i;
		person = p;
		this.balance = 0;
		PIN = abs(m.nextInt() % 9000) + 1000;
		interest = balance * 1.6 / 100;

	}

	/**
	 * sets the value of the balance of the account to balance
	 * 
	 * @param balance
	 *            value to set balance to
	 */
	public void setBalance(int balance) {
		this.balance = balance;
		interest = balance * 1.6 / 100;
	}

	/**
	 * simulates the deposit action to the account
	 * 
	 * @param sum
	 *            sum to deposit into the account
	 */
	public void deposit(int sum) {
		assert sum > 0;
		balance += sum;
		interest = balance * 1.6 / 100;
		assert balance > 0;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	/**
	 * simulates the action of withdrawing from the account
	 * 
	 * @param sum
	 *            sum to withdraw from the account
	 */
	public void withdraw(int sum) {
		assert sum < balance;
		balance -= sum;
		assert balance > 0;
	}

	/**
	 * information contained by the object into a string representation
	 */
	public String toString() {
		return "\nSaving Account:\n\tInterest=" + interest + "\n\tID:" + ID + "\n\tPIN:" + PIN + "\n\tBalance:"
				+ balance;
	}

}
