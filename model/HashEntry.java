package model;

import java.io.Serializable;
import java.util.LinkedList;

@SuppressWarnings("serial")
public class HashEntry implements Serializable {
	private int key;
	private LinkedList<Account> value;

	/**
	 * constructor
	 * 
	 * @param key
	 *            key by which the object is inserted in the entry
	 * @param value
	 *            account to be inserted
	 */
	public HashEntry(int key, LinkedList<Account> value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * constructor
	 * 
	 * @param key
	 *            of the account
	 */
	public HashEntry(int key) {
		this.key = key;
		value = new LinkedList<Account>();
	}

	/**
	 * returns the key by which the account was inserted
	 * 
	 * @return key of the entry
	 */
	public int getKey() {
		return key;
	}

	/**
	 * get value of the entry
	 * 
	 * @param index
	 *            index of the entry
	 * @return account at index
	 */
	public Account getValue(int index) {
		return value.get(index);
	}

	/**
	 * adds account o to the entry
	 * 
	 * @param o
	 *            account to be added
	 */
	public void add(Account o) {
		value.add(o);
	}

	/**
	 * returns size- number of collisions at this key
	 * 
	 * @return
	 */
	public int size() {
		return value.size();
	}

	/**
	 * removes account from entry
	 * 
	 * @param o
	 *            account to be removed
	 */
	public void remove(Account o) {
		value.remove(value.indexOf(o));
	}

	/**
	 * removes account at index index
	 * 
	 * @param index
	 *            index of the account to be removed
	 */
	public void remove(int index) {
		value.remove(index);
	}

	/**
	 * returns true if entry contains account o and false if not
	 * 
	 * @param o
	 *            account to be checked
	 * @return true or false
	 */
	public boolean contains(Account o) {
		return value.contains(o);
	}

	/**
	 * returns a string representation of the information contained by the
	 * object
	 */
	public String toString() {
		String s = "";
		for (int i = 0; i < value.size(); i++) {
			if (value.get(i) != null)
				s += value.get(i).toString() + "   ";
		}
		return s;
	}

}
