package model;

import static java.lang.Math.abs;

import java.io.Serializable;
import java.util.Random;

@SuppressWarnings("serial")
public class Account implements Serializable {
	protected int ID;
	protected Person person;
	protected int PIN;
	protected int balance;

	/**
	 * Constructor
	 */
		public Account() {

		}
	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person awning the account
	 * @param balance
	 *            the sum present in the account
	 */
	public Account(int ID, Person person, int balance) {
		this.ID = ID;
		this.person = person;
		this.balance = balance;
		Random rand = new Random();
		this.PIN = abs(rand.nextInt() % 9000) + 1000;
	}

	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person owning the account
	 */
	public Account(int i, Person p) {
		Random m = new Random();
		ID = i;
		person = p;
		this.balance = 0;
		PIN = abs(m.nextInt() % 9000) + 1000;
	}

	/**
	 * return the id of the account
	 * 
	 * @return account id
	 */
	public int getID() {
		return ID;
	}

	/**
	 * sets the value of the account id to iD
	 * 
	 * @param iD
	 *            value to set the id to
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * returns the pin of the account
	 * 
	 * @return pin
	 */
	public int getPIN() {
		return PIN;
	}

	/**
	 * sets the value of the PIN of the account
	 * 
	 * @param PIN
	 *            value to set the PIN to
	 */
	public void setPIN(int PIN) {
		this.PIN = PIN;
	}

	/**
	 * returns the balance of the account
	 * 
	 * @return balance of the account
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * sets the value of the balance of the account to balance
	 * 
	 * @param balance
	 *            value to set balance to
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}

	/**
	 * simulates the deposit action to the account
	 * 
	 * @param sum
	 *            sum to deposit into the account
	 */
	/**
	 * returns the person owning the account
	 * 
	 * @return person who owns the account
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * sets value of the person owning the account to person
	 * 
	 * @param person
	 *            value to set the person to
	 */
	public void setPerson(Person person) {
		this.person = person;
	}

	public void deposit(int sum) {
		balance += sum;
	}

	/**
	 * simulates the action of withdrawing from the account
	 * 
	 * @param sum
	 *            sum to withdraw from the account
	 */
	public void withdraw(int sum) {
		assert sum < balance;
		balance -= sum;
	}

	/**
	 * information contained by the object into a string representation
	 */
	public String toString() {
		return "\n ID=" + ID + "\nperson=" + person + "\nPIN=" + PIN + "\nbalance=" + balance;
	}

}
