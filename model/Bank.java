package model;

import java.io.Serializable;
import java.util.Observable;

@SuppressWarnings("serial")
public class Bank extends Observable implements BankProc,Serializable{
	private HashTable table;
	public Bank()
	{
		
	}
	/**
	 * constructor
	 * 
	 * @param t
	 *            hashtable storing the accounts
	 */
	public Bank(HashTable t) {
		table = t;
		assert isWellFormed();
	}
	/**
	 * @pre table!=null && isWellFormed()
	 * @post @nochange
	 * @return the table
	 */
	public HashTable getTable()
	{
		assert isWellFormed();
		return table;
	}
	/**
	 * @pre table!=null && isWellFormed()
	 * @post isWellFormed()
	 * @param table
	 *            the table to set
	 */
	public void setTable(HashTable table) {
		assert isWellFormed();
		this.table = table;
		assert isWellFormed();
	}
	/**
	 * @pre isWellFormed()
	 * @pre a!=null && table.contains(a)==null
	 * @post isWellFormed()
	 * @post table.contains(a)==null
	 * @post getDimension() == getDimension()@pre + 1 
	 * adds account a to the bank(from hashtable storing all the accounts)
	 */
	public void addAccount(Account a){
		assert isWellFormed();
		assert a!=null;
		assert !table.contains(a.getID(), a);
		int d=table.getDimension();
		table.add(a.getID(), a);
		
		assert !table.contains(a.getID(), a);
		assert d==table.getDimension()-1;
		assert isWellFormed();
		this.setChanged();
		this.notifyObservers();
		
	}
	/**
	 * @pre isWellFormed()
	 * @pre a!=null && table.contains(a)!=null
	 * @post isWellFormed()
	 * @post table.contains(a)
	 * @post getDimension() == getDimension()@pre - 1 removes account a from
	 *       bank (from hashtable storing all the accounts)
	 */
	public void removeAccount(Account a)
	{
		assert isWellFormed();
		assert a!=null;
		assert !table.contains(a.getID(), a);
		int d=table.getDimension();
		table.remove(a.getID(), a);
		assert table.contains(a.getID(), a) == false;
		assert d == table.getDimension() + 1;
		assert isWellFormed();
		this.setChanged();
		this.notifyObservers();
	}
	/**
	 * @pre isWellFormed()
	 * @pre b.getID() == a.getID()
	 * @pre b!=null && table.contains(a)!=null
	 * @post isWellFormed()
	 * @post table.contains(b)!=null updates the information in the account a
	 *       stored in the bank with the information from account b
	 */
	public void updateAccount(Account a,Account b)
	{
		assert isWellFormed();
		assert table.contains(a.getID(), a);
		assert b.getID() == a.getID();
		assert b!=null;
		a=b;
		assert isWellFormed();
		assert table.contains(b.getID(), b);
		assert isWellFormed();
		this.setChanged();
		this.notifyObservers();
	
	}
	/**
	 * @pre isWellFormed()
	 * @post @nochange returns account with the id id
	 */
	public Account getAccount(int id) {
		assert isWellFormed();
		return table.get(id);
	}
	/**
	 * return true if the bank object is consistent and false if not
	 */
	public boolean isWellFormed() {
		return table.isWellFormed();
	}
	public String toString() {
		return "Bank [table=" + table + "]";
	}
	
	

}
