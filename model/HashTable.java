package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class HashTable implements Serializable {
	private HashEntry[] table;
	private static final int SIZE = 17;
	private int dimension;

	/**
	 * @return the table
	 */
	public HashEntry[] getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(HashEntry[] table) {
		this.table = table;
	}

	/**
	 * @return the dimension
	 */
	public int getDimension() {
		return dimension;
	}

	/**
	 * @param dimension
	 *            the dimension to set
	 */
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	/**
	 * constructor
	 */
	public HashTable() {
		table = new HashEntry[SIZE];
		for (int i = 0; i < SIZE; i++)
			table[i] = null;
		dimension = 0;
	}

	/**
	 * adds a key to the hashtable at the key key
	 * 
	 * @param key
	 *            key at which the account is added
	 * @param o
	 *            account to be added
	 */
	public void add(int key, Account o) {
		int hash = hashFunction(key);
		HashEntry entry = table[hash];

		if (entry == null) {
			table[hash] = new HashEntry(hash);
			table[hash].add(o);
			dimension++;
		} else {
			table[hash].add(o);
			dimension++;
		}

	}

	/**
	 * removes account from the hashtable
	 * 
	 * @param key
	 *            key from which the account is removed
	 * @param o
	 *            account to be removed
	 */
	public void remove(int key, Account o) {
		int hash = hashFunction(key);
		HashEntry entry = table[hash];
		entry.remove(o);
		dimension--;
	}

	/**
	 * checks if the table contains the account o at the key key
	 * 
	 * @param key
	 *            key at which to look for the account
	 * @param o
	 *            account which is checked
	 * @return true of the table contains the account or false if not
	 */
	public boolean contains(int key, Account o) {
		int hash = hashFunction(key);
		HashEntry entry = table[hash];
		return entry.contains(o);
	}

	/**
	 * returns the account with id id
	 * 
	 * @param id
	 *            id of the account
	 * @return the account if it is found or null if not
	 */
	public Account get(int id) {
		Account account;
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null)
				for (int j = 0; j < table[i].size(); j++) {
					account = table[i].getValue(j);
					if (account.getID() == id)
						return account;
				}
		}
		return null;
	}

	/**
	 * hash function by which the elements are added to the table
	 * 
	 * @param key
	 *            key on which the function is applied
	 * @return result of the function
	 */
	private int hashFunction(int key) {
		return (key % SIZE);
	}

	/**
	 * returns a string representation of the information stored in the object
	 */
	public String toString() {
		String s = "";

		for (int i = 0; i < table.length; i++) {
			if (table[i] != null)
				s += "\n" + table[i].getKey() + "  " + table[i].toString() + " \n";
		}
		return s;
	}

	public boolean isWellFormed() {
		int k = 0;
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null)
				for (int j = 0; j < table[i].size(); j++) {
					k++;
					if (hashFunction(table[i].getValue(j).getID()) != i)
						return false;
				}
		}
		if (k != dimension)
			return false;
		return true;
	}
}
