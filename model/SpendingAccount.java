package model;

import static java.lang.Math.abs;

import java.io.Serializable;
import java.util.Random;

@SuppressWarnings("serial")
public class SpendingAccount extends Account implements Serializable {
	private int maxWithdraw;

	public SpendingAccount() {

	}

	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person awning the account
	 * @param balance
	 *            the sum present in the account
	 * @param max
	 *            max sum the owner can withdraw at once
	 */
	public SpendingAccount(int i, Person p, int balance, int max) {
		Random rand = new Random();
		ID = i;
		this.balance = balance;
		person = p;
		PIN = abs(rand.nextInt() % 9000) + 1000;
		maxWithdraw = max;
	}

	/**
	 * Constructor
	 * 
	 * @param i
	 *            account id
	 * @param p
	 *            person awning the account
	 * @param max
	 *            max sum the owner can withdraw at once
	 */
	public SpendingAccount(int i, Person p, int max) {
		Random m = new Random();
		ID = i;
		this.balance = 0;
		person = p;
		PIN = abs(m.nextInt() % 9000) + 1000;
		maxWithdraw = max;
	}

	/**
	 * sets the value of the balance of the account to balance
	 * 
	 * @param balance
	 *            value to set balance to
	 */
	public void setBalance(int balance) {
		this.balance = balance;

	}

	/**
	 * simulates the deposit action to the account
	 * 
	 * @param sum
	 *            sum to deposit into the account
	 */
	public void deposit(int sum) {
		assert sum > 0;
		balance += sum;
		assert balance > 0;
	}

	/**
	 * simulates the action of withdrawing from the account
	 * 
	 * @param sum
	 *            sum to withdraw from the account
	 */
	public void withdraw(int sum) {
		assert sum < maxWithdraw;
		balance -= sum;
		assert balance > 0;
	}

	/**
	 * returns max sum that owner can withdraw at once
	 * 
	 * @return max withdraw sum
	 */
	public int getMaxWithdraw() {
		return maxWithdraw;
	}

	/**
	 * sets max withdraw sum to maxWithdraw
	 * 
	 * @param maxWithdraw
	 *            max sum to be withdrawn
	 */
	public void setMaxWithdraw(int maxWithdraw) {
		this.maxWithdraw = maxWithdraw;
	}

	/**
	 * information contained by the object into a string representation
	 */
	public String toString() {
		return "\nSpendingAccount: \n\tmaxWithdraw:" + maxWithdraw + "\n\tID:" + ID + "\n\tPIN:" + PIN + "\n\tBalance:"
				+ balance;
	}

}
