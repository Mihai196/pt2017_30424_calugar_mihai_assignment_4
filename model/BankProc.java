package model;
public interface BankProc {

	void addAccount(Account a);

	void removeAccount(Account a);

	public Account getAccount(int id);

	void updateAccount(Account a, Account b);

	boolean isWellFormed();
}
